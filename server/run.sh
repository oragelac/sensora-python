#!/bin/bash

docker run -itd --name wt2-sensora-python \
 --restart=always \
 --net containers_lightyear --ip 23.20.2.142 \
 -v "${PWD}/etc":"/sensora-python/etc" \
 -v "${PWD}/log":"/var/log/sensora-python" \
 -p 4321:4321 \
 wt2/sensora-python:latest
