import logging
import mysql.connector
import sys

from mysql.connector import errorcode

class DatabaseConnector:

    def __init__(self, dbInfos):

        self.connection = None
        self.dbInfos = dbInfos

    def connect(self):

        try:

            self.connection = mysql.connector.connect(user = self.dbInfos['user'],
                                            password = self.dbInfos['password'],
                                            host = self.dbInfos['host'],
                                            database = self.dbInfos['database'])

        except KeyError as e:

            logging.error('[CLIENT THREAD] database connection failed - missing attribute in configuration file : ' + str(e))
            print('Missing attribute in configuration file : ' + str(e))
            self.connection = None

        except mysql.connector.Error as e:

            logging.error('[MYSQL] ' + str(e))


    def close(self):

        if self.connection != None:

            try:

                self.connection.close()

            except mysql.connector.Error as e:

                logging.error('[MYSQL] ' + str(e))

    def fetchOne(self, table, column, value):

        if self.connection != None:

            try:

                cursor = self.connection.cursor()

            except mysql.connector.Error as e:

                logging.error('[MYSQL] ' + str(e))
                return (0, None)

            if(isinstance(value, str)):

                value = '\'' + value + '\''

            query = "SELECT * FROM " + table + " WHERE " + column + " = " + str(value)

            try:

                cursor.execute(query)
                row = cursor.fetchone()
                numberOfRows = cursor.rowcount
                cursor.close()
                return (numberOfRows, row)

            except mysql.connector.Error as e:

                logging.error('[MYSQL] ' + str(e))

        return (0, None)

    def insertData(self, table, datavalue, datetimeString, sensowardId):

        if self.connection == None:

            return False

        try:

            cursor = self.connection.cursor()

        except mysql.connector.Error as e:

            logging.error('[MYSQL] ' + str(e))

        if table == 'Son':

            row = 'decibel'

        if table == 'CarSpeed':

            row = 'speed'

        addData = ("INSERT INTO " + table + " (" + row + ", time, idRaspberry) "
                    "VALUES (%s, %s, %s)")



        data = (str(datavalue), datetimeString, sensowardId)

        try:

            cursor.execute(addData, data)
            self.connection.commit()
            cursor.close()
            return True

        except mysql.connector.Error as e:

            logging.error('[MYSQL] ' + str(e))
            return False
