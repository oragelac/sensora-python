import logging
import configparser
import sys

from sensora.clienthandler import ClientHandler

def setupConfig(filepath):

    config = configparser.ConfigParser(allow_no_value = True)

    try:

        with open(filepath) as configFile:

            config.read_file(configFile)
            return config

    except IOError as e:

        print("Error while opening config file : " + str(e))
        sys.exit()

def setupLogger(filepath):

    logging.basicConfig(level = logging.DEBUG,
                        format = '%(asctime)s %(levelname)-8s %(message)s',
                        datefmt = '%d-%m-%y %H:%M:%S',
                        filename = filepath,
                        filemode = 'a+')

def run(serversocket, config):

    threads = []

    if 'database' not in config.keys():

        logging.error('[MAIN THREAD] missing attribute in configuration file : \'clientPort\'')
        print('Missing attribute in configuration file : \'database\'')
        serversocket.close()
        logging.info('[MAIN THREAD] server successfully closed')
        sys.exit()

    if 'clientPort' not in config['connection'].keys():

        clientPort = None

    else:

        clientPort = int(config['connection']['clientPort'])

    try:

        logging.info('[MAIN THREAD] waiting for a client to connect...')
        clientConnection, clientAddress = serversocket.accept('ssl' in config['connection'].keys(), config['connection']['certificate'], config['connection']['key'])
        logging.info('[MAIN THREAD] a new client has connected : ' + str(clientAddress))
        t = ClientHandler(clientConnection, clientAddress, config['database'], clientPort)
        t.start()
        threads.append(t)

    except KeyError as e:

        logging.error('[MAIN THREAD] missing attribute in configuration file : ' + str(e))
        print('Missing attribute in configuration file : ' + str(e))
        serversocket.close()
        logging.info('[MAIN THREAD] server successfully closed')
        sys.exit()

    except KeyboardInterrupt:

        logging.info('[MAIN THREAD] closing client connections...')

        # Waiting for all the threads to end
        for thread in threads:

            thread.join()

        # Closing socket
        serversocket.close()
        logging.info('[MAIN THREAD] server successfully closed')
        sys.exit()
