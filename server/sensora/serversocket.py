import socket
import ssl
import logging
import sys


class Socket:

    def __init__(host, port, backlog = 32):

        self.backlog = backlog
        self.host = host
        self.port = port
        self.socket = None

    def bind(self):

        # Binding socket to a address
        self.socket.bind((self.host, self.localPort))

    def connect(self):

        self.socket.connect((self.server, self.port))

    def listen(self):

        # Listening
        self.socket.listen(self.backlog)

    def send(self, data):

        try:

            self.socket.send(data.encode())

        except socket.error as e:

            print(str(e))

    def receive(self, size = 1024):

        try:

            return self.socket.recv(size).decode()

        except socket.error as e:

            print(str(e))

    def close(self):

        self.socket.close()

    def accept(self, enableSsl = False, certificate = None, key = None):

        conn, addr = self.socket.accept()

        if enableSsl:

            conn = ssl.wrap_socket(conn, server_side = True, certfile = certificate, keyfile = key, ssl_version = ssl.PROTOCOL_TLSv1)

        clientConnection = ServerSocket(conn)
        return (clientConnection, addr)

class ServerSocket(Socket):

    def __init__(self, sock = None, config = None):


        if config != None:

            try:

                self.host = config['host']
                self.localPort = int(config['port'])

                if 'backlog' in config.keys():

                    self.backlog = int(config['backlog'])

                else:

                    self.backlog = 32

                # Creating socket
                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

                self.initialize()

            except KeyError as e:

                logging.error('[MAIN THREAD] missing attribute in configuration file : ' + str(e))
                print('Missing attribute in configuration file : ' + str(e))
                sys.exit()

        if(sock != None and config == None):

            self.socket = sock
            self.backlog = None


    def initialize(self):

        self.bind()
        self.listen()
