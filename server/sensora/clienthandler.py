import threading
import logging

from .database import DatabaseConnector
from .message import Data
from .message import *

class ClientHandler(threading.Thread):

    def __init__(self, connection, address, dbInfos, clientPort):

        threading.Thread.__init__(self)

        self.connection = connection
        self.address = address
        self.dbInfos = dbInfos
        self.clientPort = clientPort
        self.close = False

    def requestId(self):

        self.connection.send(Message(MessageCode.AUTH_REQUEST).encode())

    def getId(self):

        m = self.connection.receive()
        print(m)
        message = Message.decode(m)

        try:

            self.id = int(message.data().getData())
            logging.info('[CLIENT THREAD] id for client ' + str(self.address) + ' = ' + str(self.id))
            return True

        except TypeError as e:

            logging.info('[CLIENT THREAD] id for client ' + str(self.address) + ' is not valid')
            return False

        except ValueError as e:

            logging.info('[CLIENT THREAD] id for client ' + str(self.address) + ' is not valid')
            return False


    def requestData(self):

        self.connection.send(Message(MessageCode.DATA_REQUEST).encode())
        logging.info('[CLIENT THREAD] asking data to client ' + str(self.address))

    def getData(self):

        m = self.connection.receive()
        print(m)
        message = Message.decode(m)

        if message.code() == MessageCode.CLOSE_CONNECTION:

            self.close = True
            return

        self.data = message.data().getData()
        self.datetime = message.data().getDate()
        self.table = message.data().getType()
        logging.info('[CLIENT THREAD] data = ' + str(self.data))
        logging.info('[CLIENT THREAD] date = ' + self.datetime)
        logging.info('[CLIENT THREAD] type = ' + self.table)

    def sendAuthFailureMessage(self, reason):

        logging.warning('[CLIENT THREAD] unable to authenticate client ' + str(self.address))
        self.connection.send(Message(MessageCode.AUTH_FAILURE, Data(reason)).encode())

    def run(self):

        if self.clientPort == self.address[1] or self.clientPort == None:

            self.requestId()

            if self.getId():

                database = DatabaseConnector(self.dbInfos)
                database.connect()
                (numberOfRows, data) = database.fetchOne('Raspberry', 'id', self.id)

                if numberOfRows == 1:

                    self.address += (self.id,)

                    while(True):

                        self.requestData()
                        self.getData()

                        if self.close:

                            break

                        if not database.insertData(self.table, self.data, self.datetime, self.id):

                            logging.error('[CLIENT THREAD] unable to insert data from client ' + str(self.address) + ' into database')

                    database.close()

                else:

                    self.sendAuthFailureMessage('Client id not found')

            else:

                self.sendAuthFailureMessage('Invalid client id')

        else:

            self.sendAuthFailureMessage('Wrong client port')



        # Closing client connection
        logging.info('[CLIENT THREAD] closing connection with client ' + str(self.address))
        self.connection.close()
        logging.info('[CLIENT THREAD] client ' + str(self.address) + ' connection closed')
