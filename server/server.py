import sys
import os

from sensora.utils import *
from sensora.serversocket import ServerSocket

def main():

    if len(sys.argv) != 2:

        print("Error : Wrong number of parameters.\nUsage : python3 server.py configuration_file_path")
        sys.exit()

    config = setupConfig(sys.argv[1])

    try:

        setupLogger(config['log']['filepath'])

    except KeyError:

        print("Error while configuring logging module : specify logfile path in the configuration file" )
        sys.exit()

    serversocket = ServerSocket(None, config['connection'])
    logging.info('[MAIN THREAD] starting server...')

    while True:

        run(serversocket, config)


if __name__ == "__main__":

    main()
