# Sensora Python #


## What is this repository for? ##

This repository contains the client-server architecture of the Sensora project.

## How do I get set up? ##

### Client setup ###

The client script is meant to be installed on a Sensoward.
A Sensoward represents a Raspberry Pi on which various sensors have been installed on.


#### Configuration ####

Source code presented on this repository includes a basic INI configuration file.
This file can be modified to fit your preferences.

#### Dependencies ####

* Python 3
* OpenCV 2
* PiCamera
* PyAudio
* Numpy

#### Deployment instructions ####

After installing all the required dependencies, you can run the client using the following command :

```bash
python3 client.py /path/to/configuration/file
```

### Server setup ###

#### Configuration ####

Source code presented on this repository includes a basic INI configuration file.
This file can be modified to fit your preferences.

#### Dependencies ####

* Python 3
* MySql Python connector
* Docker

#### Deployment instructions ####

Server deployement can be simply done using Docker.
To start deploying server, enter the following commands :

```bash
docker build -t wt2/sensora-python /path/to/dockerfile
./run.sh 
```

#### Who do I talk to? ####

You can contact the Sensora team using the following email address : [admin@sens.oragelac.com](mailto:admin@sens.oragelac.com)