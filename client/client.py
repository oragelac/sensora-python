import configparser
import logging
import sys
import datetime
import threading
import queue

from sensora.clientsocket import ClientSocket
from sensora.sensor import *
from sensora.data import Data
from sensora.message import *

def main():

    if len(sys.argv) != 2:

        print("Error : Wrong number of parameters.\nUsage : python3 client.py configuration_file_path")
        sys.exit()

    config = configparser.ConfigParser(allow_no_value = True)
    config.read(sys.argv[1])

    try:

        logging.basicConfig(level = logging.DEBUG,
                            format = '%(asctime)s %(levelname)-8s %(message)s',
                            datefmt = '%d-%m-%y %H:%M:%S',
                            filename = config['log']['filepath'],
                            filemode = 'a+')

    except KeyError:

        print("Error while configuring logging module : specify logfile path in the configuration file" )
        sys.exit()

    # Creating socket
    clientsocket = ClientSocket(config['connection'], 'ssl' in config['connection'].keys())

    (connected, message) = clientsocket.initialize()
    logging.info(message)

    dataQueue = queue.Queue()

    sensorThreads = SensorFactory(config, dataQueue).sensorThreads()

    for thread in sensorThreads:

        thread.start()

    if(connected):

        while(True):

            m = clientsocket.receive()
            print(m)
            message = Message.decode(m)


            if message.code() == MessageCode.AUTH_REQUEST:

                try:
                    newMessage = Message(MessageCode.AUTH_ANSWER, Data(config['ward']['id']))

                except KeyError as e:

                    logging.error('[MAIN THREAD] missing attribute in configuration file : ' + str(e))
                    print('Missing attribute in configuration file : ' + str(e))
                    newMessage = Message(MessageCode.AUTH_ANSWER, Data('-1'))


                clientsocket.send(newMessage.encode())
                logging.info('Sending id to server')

            if message.code() == MessageCode.AUTH_FAILURE:

                logging.error('Authentication failed')
                logging.info('Connection closed by server')
                break

            if message.code() == MessageCode.CLOSE_CONNECTION:

                logging.info('Connection closed by server')
                break

            if message.code() == MessageCode.DATA_REQUEST:

                stop = True

                for thread in sensorThreads:

                    if not thread.jobDone():

                        stop = False


                if dataQueue.empty() and stop:

                        newMessage = Message(MessageCode.CLOSE_CONNECTION)
                        clientsocket.send(newMessage.encode())
                        break

                else:

                        data = dataQueue.get()

                newMessage = Message(MessageCode.DATA_ANSWER, Data(data[0], data[1], data[2]))
                clientsocket.send(newMessage.encode())
                logging.info('Sending data to server')

        # Close socket
        logging.info('Closing connection...')
        clientsocket.close()

    else:

        logging.warning('Unable to reach server. Closing client...')

if __name__ == "__main__":

    main()
