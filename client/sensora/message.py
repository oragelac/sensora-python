import json
import logging

from .data import Data

class MessageCode:

    CLOSE_CONNECTION = 10
    AUTH_FAILURE = 11
    AUTH_REQUEST = 12
    AUTH_ANSWER = 13
    DATA_FAILURE = 21
    DATA_REQUEST = 22
    DATA_ANSWER = 23

class Message:

    def __init__(self, code = 0, data = None):

        self.content = dict()
        self.content['code'] = code
        self.content['data'] = None

        if data != None:

            self.content['data'] = data.__dict__

    def code(self):

        return self.content['code']

    def data(self):

        if self.content['data'] != None:

            data = self.content['data']
            return Data(data['data'], data['type'], data['date'])

        return Data()

    def setCode(self, code):

        self.content['code'] = code

    def encode(self):

        return json.dumps(self.content, sort_keys = True, separators=(', ', ' : '))

    @staticmethod
    def decode(jsonstr):

        try:

            message = json.loads(jsonstr)
            code = message['code']

            if message['data'] == None:

                return Message(code)

            data = message['data']['data']
            datatype = message['data']['type']
            date = message['data']['date']
            return Message(code, Data(data, datatype, date))


        except ValueError as e:

            logging.info('[JSON DECODER] Invalid JSON string :  ' + str(e))

        except KeyError as e:

            logging.info('[JSON DECODER] Missing key in JSON string :  ' + str(e))

        return Message()
