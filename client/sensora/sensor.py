import pyaudio
import math
import audioop
import datetime
import time
import random
import abc
import threading
import logging
from picamera.array import PiRGBArray
from picamera import PiCamera
import numpy
import cv2

class SensorFactory:

    def __init__(self, config, queue):

        self.sensorThreadList = []

        for key in config['sensors'].keys():

            if key == 'random':

                self.sensorThreadList.append(RandomGenerator(config['random'], queue))

            elif key == 'audio':

                self.sensorThreadList.append(AudioSensor(config['audio'], queue))

            elif key == 'radar':

                self.sensorThreadList.append(RadarSensor(config['radar'], queue))

            else:

                pass

    def sensorThreads(self):

        return self.sensorThreadList

class Sensor(abc.ABC, threading.Thread):

    def __init__(self, queue):

        threading.Thread.__init__(self)
        self.queue = queue
        self.recordedSamples = 0


    @abc.abstractmethod
    def run(self):

        pass

    @abc.abstractmethod
    def jobDone(self):

        pass

class RandomGenerator(Sensor):

    def __init__(self, config, queue):

        Sensor.__init__(self, queue)

        try:

            self.min = int(config['min'])
            self.max = int(config['max'])
            self.samples = int(config['samples'])
            self.recordedSamples = 0
            self.runnable = True

        except KeyError as e:

            logging.error('[RANDOM GENERATOR] missing attribute in configuration file : ' + str(e))
            print('Missing attribute in configuration file : ' + str(e))
            self.runnable = False

    def run(self):

        if self.runnable:

            for i in range(self.samples):

                self.queue.put([random.uniform(self.min, self.max), datetime.datetime.now(), 'Son'])
                time.sleep(0.01)
                self.recordedSamples += 1

    def jobDone(self):

        return self.recordedSamples == self.samples

class AudioSensor(Sensor):

    def __init__(self, config, queue):

        try:

            Sensor.__init__(self, queue)

            self.chunk = 8192
            self.format = pyaudio.paInt16
            self.channels = 1
            self.sampleRate = 44100
            self.recordRate = int(config['recordRate'])
            self.samples = int(config['samples'])
            self.audio = pyaudio.PyAudio()
            self.recordedSamples = 0
            self.runnable = True

        except KeyError as e:

            logging.error('[AUDIO SENSOR] missing attribute in configuration file : ' + str(e))
            print('Missing attribute in configuration file : ' + str(e))
            self.runnable = False

    def run(self):

        if self.runnable:

            for i in range(self.samples):

                self.stream = self.audio.open(format = self.format,
                                        channels = self.channels,
                                        rate = self.sampleRate,
                                        input = True,
                                        frames_per_buffer = self.chunk)
                sample = self.stream.read(self.chunk)
                sample = 20 * math.log(audioop.rms(sample, 2), 10)
                self.queue.put([sample, datetime.datetime.now(), 'Son'])
                self.recordedSamples += 1
                self.stream.close()
                self.skip()

    def skip(self):

        count = 0

        while count < int(self.recordRate * self.sampleRate):
            self.stream = self.audio.open(format = self.format,
                                        channels = self.channels,
                                        rate = self.sampleRate,
                                        input = True,
                                        frames_per_buffer = self.chunk)
            self.stream.read(self.chunk)
            count += self.chunk
            time.sleep(0.01)
            self.stream.close()

    def jobDone(self):

        return self.recordedSamples == self.samples

class RadarSensor(Sensor) :


    def __init__(self, config, queue):

        Sensor.__init__(self, queue)

        self.runnable = True

        try:

            self.DISTANCE = int(config['distance'])
            self.samples = int(config['samples'])

            self.THRESHOLD = 15
            self.MIN_AREA = 175
            self.BLURSIZE = (15,15)
            self.IMAGEWIDTH = 640
            self.IMAGEHEIGHT = 480
            self.RESOLUTION = [self.IMAGEWIDTH,self.IMAGEHEIGHT]
            self.FOV = 53.5
            self.FPS = 30
            self.WAITING = 0
            self.TRACKING = 1
            self.UNKNOWN = 0
            self.LEFT_TO_RIGHT = 1
            self.RIGHT_TO_LEFT = 2
            self.state = self.WAITING
            self.direction = self.UNKNOWN
            self.initial_x = 0
            self.last_x = 0
            self.base_image = None
            self.abs_chg = 0
            self.kmh = 0
            self.secs = 0.0
            self.show_bounds = False
            self.showImage = False
            self.fx,self.fy = -1,-1
            self.ix,self.iy = 0, 0
            self.drawing = False
            self.setup_complete = False
            self.tracking = False
            self.text_on_image = 'Pas de voiture'
            self.loop_count = 0
            self.prompt = ''
            self.camera = PiCamera()
            self.camera.resolution = self.RESOLUTION
            self.camera.framerate = self.FPS
            self.camera.vflip = True
            self.camera.hflip = True
            self.rawCapture = PiRGBArray(self.camera)
            time.sleep(0.9)

            self.org_image = None
            self.image = None
            self.monitored_width = 0
            self.monitored_height = 0
            self.lower_right_x = 0
            self.lower_right_y = 0
            self.upper_left_x = 0
            self.upper_left_y = 0

            self.frame_width_ft = 2*(math.tan(math.radians(self.FOV*0.5))*self.DISTANCE)
            self.ftperpixel = self.frame_width_ft / float(self.IMAGEWIDTH)
            self.motion_found = False
            self.tb_kmh = []

            self.recordedSamples = 0

        except KeyError as e:

            logging.error('[CONSTANT GENERATOR] missing attribute in configuration file : ' + str(e))
            print('Missing attribute in configuration file : ' + str(e))
            self.runnable = False

    def run(self):

        if self.runnable:

            self.upper_left_x = 0
            self.lower_right_x = 640
            self.upper_left_y = 0
            self.lower_right_y = 480

            self.launch_capture()



    def get_speed(self, pixels, ftperpixel, secs):

        if secs > 0.0:
            return ((pixels * ftperpixel)/ secs) * 3.6
        else:
            return 0.0


    def secs_diff(self, endTime, begTime):

        diff = (endTime - begTime).total_seconds()
        return diff

    # Crop the frame to the monitored area, convert it to grayscale, and blur it
    # Crop area defined by [y1:y2,x1:x2]
    def crop_gray_blur(self) :
        gray = self.image[self.upper_left_y:self.lower_right_y,self.upper_left_x:self.lower_right_x]
        # convert it to grayscale, and blur it
        gray = cv2.cvtColor(gray, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, self.BLURSIZE, 0)
        return gray

    # Find contours and return them
    def find_contours(self, gray):
        # compute the absolute difference between the current image and
        # base image and then turn eveything lighter than THRESHOLD into
        # white
        frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(self.base_image))
        thresh = cv2.threshold(frameDelta, self.THRESHOLD, 255, cv2.THRESH_BINARY)[1]

        # dilate the thresholded image to fill in any holes, then find contours
        # on thresholded image
        thresh = cv2.dilate(thresh, None, iterations=2)
        (_, cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        return (_, cnts, _)

    def examine_contours(self, cnts):
        # look for motion
        self.motion_found = False
        biggest_area = 0
        (x, y, w, h) = (0, 0, 0, 0)
        # examine the contours, looking for the largest one
        for c in cnts:
            (x, y, w, h) = cv2.boundingRect(c)
            # get an approximate area of the contour
            found_area = w*h
            # find the largest bounding rectangle
            if (found_area > self.MIN_AREA) and (found_area > biggest_area):
                biggest_area = found_area
                self.motion_found = True

        return (x, y, w, h)

    # Initialize Tracking and clear the kmh array
    def init_tracking(self,x):
        self.state = self.TRACKING
        self.initial_x = x
        self.last_x = x
        del self.tb_kmh[:]
        self.text_on_image = 'Tracking'
        print(self.text_on_image)


    # Detect which direction the motion object is moving
    # and set abs_chg and direction depends
    def detect_direction(self, x, w) :
        if x >= self.last_x:
            self.direction = self.LEFT_TO_RIGHT
            self.abs_chg = x + w - self.initial_x
        else:
            self.direction = self.RIGHT_TO_LEFT
            self.abs_chg = self.initial_x - x

    #Return the median from an array
    def median(self,l):
        return numpy.median(numpy.array(l))

    # Return average from an array and rejecting outliers
    def average_value(self,l):
        new_l = []
        for i,member in enumerate(l):
                diff = numpy.abs(member - self.median(l))
                if(diff < 10):
                        new_l.append(member)
        average = numpy.average(numpy.array(new_l))
        return average

    def launch_capture(self) :

        for frame in self.camera.capture_continuous(self.rawCapture, format="bgr", use_video_port=True):

            timestamp = datetime.datetime.now()
            # grab the raw NumPy array representing the image
            self.image = frame.array

            gray = self.crop_gray_blur()

            # if the base image has not been defined, initialize it
            if self.base_image is None:
                self.base_image = gray.copy().astype("float")
                self.rawCapture.truncate(0)
                continue

            (_, cnts, _) = self.find_contours(gray)
            (x, y, w, h) = self.examine_contours(cnts)


            if self.motion_found:

                if self.state == self.WAITING:
                    initial_time = timestamp
                    self.init_tracking(x)

                else:
                    if self.state == self.TRACKING:
                        self.detect_direction(x,w)
                        self.secs = self.secs_diff(timestamp,initial_time)
                        self.kmh = self.get_speed(self.abs_chg,self.ftperpixel,self.secs)
                        self.tb_kmh.append(self.kmh)
                        print("--> chg={}  secs={}  kmh={} this_x={} w={} ".format(self.abs_chg,self.secs,"%.0f" % self.kmh,x,w))

            else:

                if self.state != self.WAITING:

                    self.state = self.WAITING
                    self.direction = self.UNKNOWN
                    self.text_on_image = 'Aucune voiture detectee'
                    print(self.text_on_image)
                    if self.tb_kmh :
                        print("Tableau final : {}".format(self.tb_kmh))
                        print("Kmh moyen : %4.1f" %self.average_value(self.tb_kmh))
                        self.queue.put([self.average_value(self.tb_kmh), datetime.datetime.now(), 'CarSpeed'])
                        self.recordedSamples += 1

                        if self.recordedSamples == self.samples:

                            break

            # only update image and wait for a keypress when waiting for a car
            # or if 50 frames have been processed in the WAITING state.
            # This is required since waitkey slows processing.
            if (self.state == self.WAITING) or (self.loop_count > 50):

                if self.state == self.WAITING:
                    self.last_x = 0
                    cv2.accumulateWeighted(gray, self.base_image, 0.25)

                self.state=self.WAITING;
                self.loop_count = 0

        # clear the stream in preparation for the next frame
            self.rawCapture.truncate(0)
            self.loop_count = self.loop_count + 1

    def jobDone(self):

        return self.recordedSamples == self.samples
