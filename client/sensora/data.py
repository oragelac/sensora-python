import datetime

class Data:

    def __init__(self, data = None, date = None, datatype = str()):

        self.data = data
        self.date = str(date)
        self.type = datatype

    def getData(self):

        return self.data

    def getDate(self):

        return self.date

    def getType(self):

        return self.type

    def setData(self, data):

        self.data = data

    def setDate(self, date):

        self.date = date

    def setType(self, datatype):

        self.type = datatype
