import socket
import ssl
import sys
import logging

class Socket:

    def __init__(host, port, backlog = 32):

        self.backlog = backlog
        self.host = host
        self.port = port
        self.socket = None

    def bind(self):

        # Binding socket to a address
        self.socket.bind((self.host, self.localPort))

    def connect(self):

        self.socket.connect((self.server, self.port))

    def listen(self):

        # Listening
        self.socket.listen(self.backlog)

    def send(self, data):

        self.socket.send(data.encode())

    def receive(self, size = 1024):

        return self.socket.recv(size).decode()

    def close(self):

        self.socket.close()

    def accept(self, enableSsl = False, certificate = None, key = None):

        conn, addr = self.socket.accept()

        if enableSsl:

            conn = ssl.wrap_socket(conn, server_side = True, certfile = certificate, keyfile = key)

        clientConnection = ServerSocket(conn)

        return (clientConnection, addr)

class ClientSocket(Socket):

    def __init__(self, config, enableSsl = False):

        try:

            self.server = config['server']
            self.port = int(config['port'])
            self.maxAttempts = int(config['maxAttempts'])
            self.socket = None
            self.ssl = enableSsl

            if('client' in config.keys() and 'localPort' in config.keys()):

                self.localPort = int(config['localPort'])
                self.host = config['client']

                self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                self.bind()

            else:

                self.socket = socket.socket()

            if self.ssl:

                self.cert = config['certificate']
                self.socket = ssl.wrap_socket(self.socket, ca_certs = self.cert, cert_reqs = ssl.CERT_REQUIRED)

        except KeyError as e:

            logging.error('Missing attribute in configuration file : ' + str(e))
            print('Missing attribute in configuration file : ' + str(e))
            sys.exit()


    def initialize(self):

        attempts = 0
        message = ''

        while(attempts < self.maxAttempts):

            try:
                attempts += 1
                self.connect()

                return (True, 'Successfully connected to server')

            except socket.error as exception:

                    message = 'Could not connect to server : ' + str(exception)

        return (False, message)
